# UXPin Zadanie rekrutacyjne

Sign up at https://www.uxpin.com and create a free trial account. Then test following things: 
1) check if user can log in at https://app.uxpin.com 
2) check if it's possible to create a new project?
3) check if box will be created when user click on “Box” icon in library section on the left?
4) check if preview mode works and shows your design at https://preview.uxpin.com
5) check if it is possible to add and edit comment in Preview

# Uruchamianie
Projekt Eclipse + Gradle

Budowanie z terminala
```
gradle wrapper
./gradlew test -Dwebdriver.chrome.driver="" -Duser="" -Dpassword=""
```

Nieaktualny Gradle wyrzuci błąd.
# Uwagi powdrożeniowe 

Literówki w kodzie były dużym problemem, duże parcie na ortografie...
