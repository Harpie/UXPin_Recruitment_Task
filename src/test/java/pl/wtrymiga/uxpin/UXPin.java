package pl.wtrymiga.uxpin;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.UnhandledAlertException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import pl.wtrymiga.uxpin.WebDriverWaitExtended.Behaviour;

public class UXPin {
	private static ChromeDriver driver;
	private static Util util;
	private static WebDriverWaitExtended wait;

	private static String name;
	private static String password;
	private static final String projectTitle = "àáâãäāăąȧǎȁȃ\\<!--" + System.currentTimeMillis() % 9999;

	@BeforeClass
	public static void openBrowser() {
		name = System.getProperty("user");
		password = System.getProperty("password");

		driver = new ChromeDriver();
		driver.manage().window().maximize();
		util = new Util(driver);
		wait = new WebDriverWaitExtended(driver, 10);
		wait.specificBehaviour(
				new Behaviour<>(UnhandledAlertException.class, () -> driver.switchTo().alert().dismiss()));
		wait.ignoring(WebDriverException.class);
	}

	@Before
	public void before() {
		driver.get("https://www.uxpin.com");
		Assert.assertTrue("Wrong page title – mainpage", driver.getTitle().contains("UXPin"));
	}

	@AfterClass
	public static void closeBrowser() {
		driver.quit();
	}

	/** Task assumptions impose single testcase */
	@Test
	public void check() {
		checkIfUserCanLogIn();
		checkIfItsPossibleToCreateANewProject();
		checkIfBoxWillBeCreatedWhenUserClickOnBoxIconInLibrarySectionOnTheLeft();
		checkIfPreviewModeWorksAndShowsYourDesignAt();
		checkIfItIsPossibleToAddAndEditCommentInPreview();
	}

	private void checkIfUserCanLogIn() {
		driver.findElement(By.linkText("Log in")).click();
		logIn();
		Assert.assertTrue("Wrong page title – dashboard", driver.getTitle().contains("Dashboard"));

		wait.until(d -> {
			d.findElement(By.className("user-avatar")).click();
			return true;
		});

		Assert.assertTrue("Wrong user header title – doesn't contain name",
				wait.until(d -> d.findElement(By.className("account-name"))).getText().contains(name));
	}

	private void checkIfItsPossibleToCreateANewProject() {
		createNewProject();
		Assert.assertTrue("Wrong project header – doesn't contain project title", wait
				.until(d -> d.findElement(By.cssSelector(".project-name-select>h2"))).getText().contains(projectTitle));
		Assert.assertTrue("Wrong page title – project title doesn't contain project name",
				driver.getTitle().contains(projectTitle));
		openEditor();
	}

	private void checkIfBoxWillBeCreatedWhenUserClickOnBoxIconInLibrarySectionOnTheLeft() {
		wait.until(d -> {
			driver.findElement(By.className("icon-general-box")).findElement(By.xpath("..")).click();
			return true;
		});
		wait.until(d -> driver.findElement(By.className("el-box")));
	}

	private void checkIfPreviewModeWorksAndShowsYourDesignAt() {
		wait.until(d -> {
			util.removeTarget(driver.findElement(By.className("preview"))).click();
			return true;
		});
		wait.until(d -> driver.findElement(By.className("el-box")));

		testProjectPreviewFullPath();
		wait.until(d -> driver.findElement(By.className("el-box")));
	}

	private void checkIfItIsPossibleToAddAndEditCommentInPreview() {
		final String testText = "Ær þæm þe Romeburg getimbred wære iiii hunde wintrum 7 hundeahtatigum";
		final String testText2 = "<!--©-->";
		driver.findElement(By.partialLinkText("Comment")).click();
		addComment(testText);
		{
			final WebElement newComment = wait
					.until(d -> d.findElement(By.cssSelector(".single-comment:not(.new-comment)")));
			Assert.assertTrue("No added comment appeared", newComment.getText().contains(testText));
			editComment(newComment, testText2);
		}

		final WebElement newComment = wait.until(d -> d
				.findElement(By.cssSelector(".single-comment:not(.new-comment) [input-text='previewText']>span")));
		Assert.assertTrue("No edited comment appeared", newComment.getText().contains(testText2));

	}

	///////////

	private void logIn() {
		driver.findElement(By.name("login")).sendKeys(name);
		driver.findElement(By.name("password")).sendKeys(password);
		driver.findElement(By.cssSelector("[type='submit']")).click();
	}

	private void createNewProject() {
		wait.until(d -> {
			d.findElement(By.linkText("New project")).click();
			return true;
		});
		{
			WebElement wrapper = driver.findElement(By.className("modal-box-wrapper"));

			wrapper.findElement(By.id("project-name")).sendKeys(projectTitle);
			wrapper.findElement(By.tagName("button")).click();
		}
	}

	private void openEditor() {
		wait.until(d -> {
			d.findElement(By.cssSelector(".start-option div>button")).click();
			return true;
		});
	}

	private void testProjectPreviewFullPath() {
		wait.until(d -> {
			d.get("https://app.uxpin.com/dashboard/projects");
			return driver.getCurrentUrl().contains("dashboard");
		});

		wait.until(d -> {
			driver.findElement(By.partialLinkText(projectTitle)).click();
			return true;
		});

		wait.until(d -> util.hover(d.findElement(By.className("inside-project-box"))));
		wait.until(d -> {
			util.removeTarget(d.findElement(By.className("preview"))).click();
			return true;
		});
	}

	private void addComment(final String testText) {
		driver.findElement(By.className("ng-canvas-container")).click();
		{
			final WebElement box = driver.findElement(By.cssSelector(".new-comment"));
			box.findElement(By.tagName("textarea")).sendKeys(testText);
			box.findElement(By.cssSelector(".submit-menu button")).click();
		}
	}

	private void editComment(final WebElement container, final String testText) {
		util.hover(container.findElement(By.className("icon-general-cog")));
		container.findElement(By.className("edit")).click();
		container.findElement(By.tagName("textarea")).sendKeys(testText);
		container.findElement(By.cssSelector(".submit-menu button")).click();
	}

}