package pl.wtrymiga.uxpin;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.support.ui.Clock;
import org.openqa.selenium.support.ui.Duration;
import org.openqa.selenium.support.ui.Sleeper;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.base.Throwables;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

//trochę popisówka z tymi refleksjami
public class WebDriverWaitExtended extends WebDriverWait {
	private final WebDriver input;
	private final Clock clock;
	private final Sleeper sleeper;

	private Duration getTimeout() {
		return (Duration) getField("timeout");
	};

	@SuppressWarnings("unchecked")
	private Supplier<String> getMessageSupplier() {
		return (Supplier<String>) getField("messageSupplier");
	};

	private Duration getInterval() {
		return (Duration) getField("interval");
	};

	@SuppressWarnings("unchecked")
	private List<Class<? extends Throwable>> getIgnoredExeption() {
		return (List<Class<? extends Throwable>>) getField("ignoredExceptions");
	}

	public WebDriverWaitExtended(WebDriver driver, long timeOutInSeconds) {
		super(driver, timeOutInSeconds);
		input = driver;
		clock = (Clock) getField("clock");
		sleeper = (Sleeper) getField("sleeper");
	}

	private List<Behaviour<?, ?>> changedExceptions = Lists.newLinkedList();

	public <K extends Throwable, L extends Exception> WebDriverWaitExtended specificBehaviourAll(
			Collection<Behaviour<K, L>> types) {
		changedExceptions.addAll(types);
		return this;
	}

	public <K extends Throwable, L extends Exception> WebDriverWaitExtended specificBehaviour(
			Behaviour<K, L> exceptionType) {
		return this.specificBehaviourAll(ImmutableList.<Behaviour<K, L>>of(exceptionType));
	}

	public <K extends Throwable, L extends Exception> WebDriverWaitExtended specificBehaviour(Behaviour<K, L> firstType,
			Behaviour<K, L> secondType) {
		return this.specificBehaviourAll(ImmutableList.of(firstType, secondType));
	}

	///////////

	@Override
	public <V> V until(Function<? super WebDriver, V> isTrue) {
		long end = clock.laterBy(getTimeout().in(MILLISECONDS));
		Throwable lastException = null;
		while (true) {
			try {
				V value = isTrue.apply(input);
				if (value != null && (Boolean.class != value.getClass() || Boolean.TRUE.equals(value))) {
					return value;
				}
				lastException = null;
			} catch (Throwable e) {
				otherBehaviour: {
					for (Behaviour<?, ?> ignoredException : changedExceptions) {
						if (ignoredException.exeption.isInstance(e))
							try {
								ignoredException.quest.run();
								break otherBehaviour;
							} catch (Throwable e1) {
								e = e1;
							}
					}

					lastException = propagateIfNotIgnored(e);
				}
			}

			if (!clock.isNowBefore(end)) {
				String message = getMessageSupplier() != null ? getMessageSupplier().get() : null;

				String timeoutMessage = String.format(
						"Expected condition failed: %s (tried for %d second(s) with %s interval)",
						message == null ? "waiting for " + isTrue : message, getTimeout().in(SECONDS), getInterval());

				throw timeoutException(timeoutMessage, lastException);
			}

			try {
				sleeper.sleep(getInterval());
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
				throw new WebDriverException(e);
			}
		}
	}

	private Throwable propagateIfNotIgnored(Throwable e) {
		for (Class<? extends Throwable> ignoredException : getIgnoredExeption()) {
			if (ignoredException.isInstance(e)) {
				return e;
			}
		}
		Throwables.throwIfUnchecked(e);
		throw new RuntimeException(e);
	}

	///////////

	private Object getField(final String name) {
		try {
			Field privateStringField = getClass().getSuperclass().getSuperclass().getDeclaredField(name);
			privateStringField.setAccessible(true);
			return privateStringField.get(this);
		} catch (NoSuchFieldException | SecurityException | IllegalArgumentException | IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}

	///////////

	@FunctionalInterface
	public interface RunnableThrowable<E extends Exception> {
		void run() throws E;
	}

	@FunctionalInterface
	public interface ConsumerThrowable<T, E extends Exception> {
		void accept(T t) throws E;
	}

	public static class Behaviour<K extends Throwable, L extends Exception> {
		public Behaviour(Class<K> exeption, RunnableThrowable<L> after) {
			this.exeption = exeption;
			this.quest = after;
		}

		public final Class<K> exeption;
		public final RunnableThrowable<L> quest;
	}

}
