package pl.wtrymiga.uxpin;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class Util {
	public Util(ChromeDriver driver) {
		this.driver = driver;
		action = new Actions(driver);
	}

	private ChromeDriver driver;
	private Actions action;

	public WebElement hover(WebElement element) {
		action.moveToElement(element).build().perform();
		return element;
	}

	public WebElement removeTarget(WebElement element) {
		driver.executeScript("arguments[0].removeAttribute('target');", element);
		return element;
	}

}
